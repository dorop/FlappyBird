﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlappyBird
{
    class Obstacle : GameObjectList
    {
        Pipe pipeTop;
        Pipe pipeBottom;
        int pipeSpaceMinimum;
        int speedHorizontal;

        public Obstacle(int layer = 0, string id = "") : base(layer, id)
        {
            pipeTop = new Pipe("pipe-green");
            pipeTop.rotation = 180 * ((float)Math.PI / 180);
            pipeTop.Mirror = true;
            pipeBottom = new Pipe("pipe-green");

            Add(pipeTop);
            Add(pipeBottom);

            speedHorizontal = 100;
            Position = new Vector2(GameEnvironment.Screen.X / 2, GameEnvironment.Screen.Y / 2);

            pipeSpaceMinimum = 100;
            velocity.X = -speedHorizontal;
            SetObstaclePositions();
        }

        public int Width
        {
            get { return pipeTop.Width > pipeBottom.Width ? pipeTop.Width : pipeBottom.Width; }
        }

        public void SetObstaclePositions()
        {
            pipeTop.Position = new Vector2(0, 0 - (pipeTop.Height / 2) - GameEnvironment.Random.Next((pipeSpaceMinimum / 2), pipeSpaceMinimum));
            pipeBottom.Position = new Vector2(0, 0 + (pipeTop.Height / 2) + GameEnvironment.Random.Next((pipeSpaceMinimum / 2), pipeSpaceMinimum));
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            position += velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;
        }

        public bool CollidesWith(SpriteGameObject obj)
        {
            return pipeTop.CollidesWith(obj) || pipeBottom.CollidesWith(obj);
        }
    }
}
