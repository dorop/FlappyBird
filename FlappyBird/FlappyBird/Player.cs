﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlappyBird
{
    class Player : SpriteGameObject
    {
        int speedFalling;

        public Player(string assetName, int layer = 0, string id = "", int sheetIndex = 0)
        : base(assetName, layer, id, sheetIndex)
        {
            origin = Center;
            Position = new Vector2(GameEnvironment.Screen.X / 2, GameEnvironment.Screen.Y / 2);
            speedFalling = 400;
        }

        public override void HandleInput(InputHelper inputHelper)
        {
            if (inputHelper.KeyPressed(Keys.Space))
            {
                velocity.Y -= speedFalling;
                if (velocity.Y < -speedFalling)
                {
                    velocity.Y = -speedFalling;
                }
            }
        }

        public override void Update(GameTime gameTime)
        {
            velocity.Y += speedFalling * (float)gameTime.ElapsedGameTime.TotalSeconds;
            base.Update(gameTime);            
        }

    }
}
